var baseData = {
  "channels": [
    {
      "id": "channel1",
      "name": "channel1",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel2",
      "name": "channel2",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel3",
      "name": "channel3",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel4",
      "name": "channel4",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel5",
      "name": "channel5",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel6",
      "name": "channel6",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    },
    {
      "id": "channel7",
      "name": "channel7",
      "description": "description text",
      "startingscore": "0",
      "knockedout": false
    }
  ],
  "questions": [
    {
      "text": "Question 1",
      "answer": null,
      "answers": [
        {
          "text": "Answer 1",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        },
		{
          "text": "Answer 2",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        },
		{
          "text": "Answer 3",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        }
      ]
    },
    {
      "text": "Question 2",
      "answer": null,
      "answers": [
        {
          "text": "Answer 1",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        }
      ]
    },
    {
      "text": "Question 3",
      "answer": null,
      "answers": [
        {
          "text": "Answer 1",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        }
      ]
    },
    {
      "text": "Question 4",
      "answer": null,
      "answers": [
        {
          "text": "Answer 1",
          "affects": [
            {
              "channelid": "channel1",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel2",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel3",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel4",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel5",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel6",
              "score": "10",
              "knockedout": false
            },
            {
              "channelid": "channel7",
              "score": "10",
              "knockedout": false
            }
          ]
        }
      ]
    }
  ]
}