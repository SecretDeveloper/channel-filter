var baseData = {
	"channels" : [{
			"id" : "planebiz",
			"name" : "Gary",
			"description" : "Web based enrollment.",
			"startingscore" : "0",
			"knockedout" : false
		}, {
			"id" : "evb",
			"name" : "EVB",
			"description" : "description text",
			"startingscore" : "0",
			"knockedout" : false
		}, {
			"id" : "link",
			"name" : "Link",
			"description" : "description text",
			"startingscore" : "0",
			"knockedout" : false
		}, {
			"id" : "hosting",
			"name" : "Hosting",
			"description" : "description text",
			"startingscore" : "0",
			"knockedout" : false
		}, {
			"id" : "exchange",
			"name" : "Exchange",
			"description" : "description text",
			"startingscore" : "0",
			"knockedout" : false
		}, {
			"id" : "paper",
			"name" : "Paper",
			"description" : "description text",
			"startingscore" : "0",
			"knockedout" : false
		}
	],
	"questions" : [{
			"text" : "Connected Partner",
			"answer" : null,
			"answers" : [{
					"text" : "Yes",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false							
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false
						}, {
							"channelid" : "link",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false
						}, {
							"channelid" : "hosting",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false
						}
					]
				}, {
					"text" : "Big long string pof stuff",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false
						}, {
							"channelid" : "link",
							"score" : "0",
							"knockedout" : false,
							"reactivate":false
						}, {
							"channelid" : "hosting",
							"score" : "10",
							"knockedout" : false,
							"reactivate":false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "10",
							"knockedout" : true,
							"reactivate":false
						}
					]
				}
			]
		}, {
			"text" : "Less than 500 lives?",
			"answer" : null,
			"answers" : [{
					"text" : "Yes",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false,
							"reactivate": false
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : false, 
							"reactivate": false
						}, {
							"channelid" : "link",
							"score" : "0",
							"knockedout" : false
							, "reactivate": false
						}, {
							"channelid" : "hosting",
							"score" : "0",
							"knockedout" : true,
							"reactivate": false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "500",
							"knockedout" : false, 							
							"reactivate":true
						}
					]
				}, {
					"text" : "No",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false, "reactivate":false
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "link",
							"score" : "0",
							"knockedout" : true,
							"reactivate":false
						}, {
							"channelid" : "hosting",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}
					]
				}
			]
		}, {
			"text" : "Technically proficient?",
			"answer" : null,
			"answers" : [{
					"text" : "Yes",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : true,
							"reactivate":false
						}, {
							"channelid" : "link",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "hosting",
							"score" : "0",
							"knockedout" : true,
							"reactivate":false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}
					]
				}, {
					"text" : "No",
					"affects" : [{
							"channelid" : "planebiz",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "evb",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "link",
							"score" : "0",
							"knockedout" : true
						}, {
							"channelid" : "hosting",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "paper",
							"score" : "10",
							"knockedout" : false, 
							"reactivate":false
						}, {
							"channelid" : "exchange",
							"score" : "10",
							"knockedout" : true,
							"reactivate":false
						}
					]
				}
			]
		}
	]
}
