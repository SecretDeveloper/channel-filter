var baseData = {
    "channels": [{
        "id": "planebiz",
            "name": "Plane.biz",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }, {
        "id": "evb",
            "name": "EVB",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }, {
        "id": "link",
            "name": "Link",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }, {
        "id": "hosting",
            "name": "Hosting",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }, {
        "id": "exchange",
            "name": "XpressLink",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }, {
        "id": "paper",
            "name": "Paper",
            "description": "",
            "startingscore": "0",
            "knockedout": false
    }],
        "questions": [{
        "text": "Is there an enrollment technology in use?",
            "answer": null,
            "answers": [{
            "qid": false,
            "text": "No",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": true,
					"reactivate":false	
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": true,
							"reactivate":false	
            }]
        }, {
            "qid": false,
            "text": "Yes - but the partner is not listed below.",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": true,
							"reactivate":false	
            }]
        }, {
            "qid": false,
            "text": "Yes - Benefits Connect",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false,
							"reactivate":false	
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": true,
							"reactivate":false	
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": true,
							"reactivate":false	
            }]
        }, {
            "qid": false,
            "text": "Yes - BSwift",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": true
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": true
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false
            }]
        },{
            "qid": false,
            "text": "American Benefits Consulting (ABC)",
			"description" : "Connected partner",
			"products" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":true,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,

				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        }, {
            "qid": false,
            "text": "Accordware (BenXpress)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        }, {
            "qid": false,
            "text": "Benefit Communications, Inc (ElectBenefits)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "BenefitFocus",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },
		{
            "qid": false,
            "text": "Benefit Harbor",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Benefit Plan Manager",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Benefit Vision",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "BenefitsCONNECT",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Benelink (BenelinkConnect)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "BeneTrac",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "BlinkHR",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Bloom Health",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "bswift",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "BusinessSolver",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Common Census (Common Benefits)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Covala",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Dominium (i-Enroller)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Empowered Benefits",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "EnrollVB (VEBA/Van Epps)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": true,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Falcon (Lock & Load)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Falcon (Lock & Load)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Farmington",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Houze & Associates",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Impact Interactive (WeCare)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Infinity",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Innotech",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": true,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": true,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Innovia Group",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "IPA (eElect, 4my Benefits & EasyEnroll)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Maxum Financial",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Mills Benefit Group",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "My Paperless Office",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "National Benefits (eBenefits)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Navitech",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "NuStar",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Personal Communications Inc",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Selerix (Selerix, Motivano, SmartEnroll, etc.)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "SmartBen",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": true,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": true,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": false,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Trion (VBConnect) ",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Westlake Financial (Benefits Talk)",
			"description" : "Connected partner",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        },{
            "qid": false,
            "text": "Worksite Benefits",
			"description" : "Connected partner",
			"products" : "Products: Acc, ISTD, TermLife, VUL, GCI",
                "affects": [{
                "channelid": "planebiz",
				"score": "40",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "evb",
				"score": "20",
				"knockedout":false,
				"reactivate":false	
            }, {
                "channelid": "link",
				"score": "50",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "hosting",
				"score": "30",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "paper",
				"score": "10",
				"knockedout": false,
				"reactivate":false	
            }, {
                "channelid": "exchange",
				"score": "60",
				"knockedout": true,
				"reactivate":false	
            }]
        }]
    }, {

        "text": "Is an enrollment partner supporting this case?",
            "answer": null,
            "answers": [{
            "qid": false,
            "text": "No",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": true					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Benefit Architects, Inc.",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": false,
            "text": "Benefit Communications Insourcing",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Benefit Communications, Inc. (BCI)",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":true
            }]
        },{
            "qid": false,
            "text": "Benefits Technologies",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Complete Benefit Alliance",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },
		{
            "qid": false,
            "text": "Complete Benefit Alliance",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Employee Family Protection",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Enrollment Advisors",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Hodges Mace",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Innotech",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":true
            }]
        },{
            "qid": false,
            "text": "Other Enrollment Partner",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "The Farmington Company",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Trion Group, Inc.",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Univers",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Willard-Block Companies",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Workplace Solutions",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        },{
            "qid": false,
            "text": "Worksite Benefit Services",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false
					,
					"reactivate":false	
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": false,
            "text": "No Bswift",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": false,
            "text": "Yes - BCI",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":true
            }]
        }, {
            "qid": false,
            "text": "Yes - Innotech",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }]
    },

    {
        "text": "Does offering contain GHI?",
            "answer": null,
            "answers": [{
             "qid": false,
            "text": "Yes",
                "affects": [{
                "channelid": "planebiz",
                    "score": "300",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "200",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "500",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "400",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "100",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "600",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": false,
            "text": "No",
                "affects": [{
                "channelid": "planebiz",
                    "score": "300",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "200",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "500",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "400",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "100",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "600",
                    "knockedout": false					,
							"reactivate":false
            }]

        }]
    },

    {

        "text": "How many eligible lives?",
            "answer": null,
            "answers": [{
             "qid": true,
            "text": "<500",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": true,
            "text": ">500",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }]
    },


    {
        "qid": "6",
        "text": "What is the enrollment strategy?",
            "answer": null,
            "answers": [{
              "qid": true,
            "text": "F2F",
                "affects": [{
                "channelid": "planebiz",
                    "score": "400",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "200",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "500",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "300",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "100",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "600",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": true,
            "text": "CC",
                "affects": [{
                "channelid": "planebiz",
                    "score": "400",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "200",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "500",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "300",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "100",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "600",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": true,
            "text": "SS",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }]
    },

    {
        "text": "Is the offer GI only?",
            "answer": null,
            "answers": [{
                "qid": true,
            "text": "Yes",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": true,
            "text": "No",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }]
    },

    {
        
        "text": "New case or re-enrollment?",
            "answer": null,
            "answers": [{
                "qid": true,
            "text": "New",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }, {
            "qid": true,
            "text": "Re-enrollment",
                "affects": [{
                "channelid": "planebiz",
                    "score": "40",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "evb",
                    "score": "20",
                    "knockedout": true					,
							"reactivate":false
            }, {
                "channelid": "link",
                    "score": "50",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "hosting",
                    "score": "30",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "paper",
                    "score": "10",
                    "knockedout": false					,
							"reactivate":false
            }, {
                "channelid": "exchange",
                    "score": "60",
                    "knockedout": false					,
							"reactivate":false
            }]
        }]
    }]
}