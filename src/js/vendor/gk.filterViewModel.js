/*
	Channel-Filter
	https://bitbucket.org/gkenneally/channel-filter
	Author: Gary Kenneally
*/
(function(){

    gk.createFilterViewModel = function () {
        'use strict';
        var self = {};

        self.init = function () {
            _.each(this.items.channels(), function (channel) {
                channel.score = ko.observable(channel.startingscore());
                channel.knockedout = ko.observable(false);
            });         
        };

        self.reset = function () {
            _.each(this.items.channels(), function (channel) {
                channel.score(0);
                channel.knockedout(false);
            });
        };

        self.calculate = function () {
            self.reset();
            _.each(self.items.questions(), function (question) {
                if (question.answer()) {
                    _.each(question.answer().affects(), function (affect) {
                        _.each(self.items.channels(), function (channel) {
                            if (affect.channelid() === channel.id()) {
                                var t = parseInt(channel.score()) + parseInt(affect.score());
                                channel.score(t);

								
                                if (affect.knockedout())
                                    channel.knockedout(true);								
								
                                if (affect.reactivate())
                                    channel.knockedout(false);

                            }
                        });
                    });
                }
            });            
        };
        
        self.loadData = function (data){
            self.items = ko.mapping.fromJS(data);
            self.init();
        },        

        self.onAnswer= function (question, answer, event) {
            question.answer(answer);
            self.calculate();
        }

        self.removeAnswer = function (question, event) {
            question.answer(null);
            self.calculate();
        }

        self.channelDisplayMode = function (channel) { 
            if (channel.knockedout())
                return 'template-channel-knockedout';
            return 'template-channel';
        }

        self.questionDisplayMode = function (question) {
		
			if(question.answer()){
				return 'template-question-answered';				
			}else{	

					return 'template-question-unanswered-ddl';
			
			}
		}

        return self;
    }
})();