/*
    Channel-Filter
    https://bitbucket.org/gkenneally/channel-filter
    Author: Gary Kenneally
*/
/*jslint browser: true nomen:true newcap:true*/
/*global gk, ko, _*/

(function () {
    'use strict';
    var root = this,

        gk = function (obj) {
            if (obj instanceof gk) {
                return obj;
            }
            if (!(this instanceof gk)) {
                return new gk(obj);
            }
        };
    gk.VERSION = '0.1.0';

    root.gk = gk;

    if (!_) {
        throw 'underscore (_) not defined, check that it is loaded.';
    }

    if (!ko) {
        throw 'knockout (ko) not defined, check that it is loaded.';
    }
// weird jslint issue with .call(this) which is explained here: https://github.com/jamesallardice/jslint-error-explanations/issues/4
}.bind(this)());